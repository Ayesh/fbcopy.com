Zepto(function($){
  $('body').removeClass('is_loading');

  var main_form = $("#url-form");
  var submit_button = main_form.find('button');
  var results = $('#results');
  var waiting = $('#waiting')
  main_form.submit(function(e){
    e.preventDefault();
    //ga('send', 'event', 'button', 'click', 'main');
    results.hide();
    waiting.show().addClass('fadeIn');
    submit_button.addClass('is-loading');
    $.ajax({
      type: 'POST',
      url: main_form.attr('action'),
      data: main_form.serialize(),
      dataType: 'json',
      context: $('body'),
      success: function(data){
        results.html(data.response_output);
      },
      error: function(xhr, type, error){
        results.html('');
        if (xhr.response !== undefined && xhr.response.length > 0) {
          var data = $.parseJSON(xhr.response);
          results.html(data.response_output);
        }
      },
      complete: function (xhr) {
        waiting.hide();
        results.show().addClass('fadeIn');
        submit_button.removeClass('is-loading');
      }
    });
  });

  var value = main_form.find("input[name=url]").val();
  if(value.length>0){
    setTimeout(function() {
      main_form.find("input[type=submit]").click();
    }, 1500);
  }
});
