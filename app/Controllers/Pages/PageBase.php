<?php

namespace Controllers\Pages;

use Controllers\Base;
use Framework\Render\Html;

abstract class PageBase extends Base {

  protected function execute() {
    $this->render->setVariable('content_title', $title = $this->getTitle());
    $this->render->setVariable('content_body', $this->getBody());
    $this->render->setVariable('page_title', $title . ' - HTTP2.Pro');
    $this->render->setVariable('sub_title', 'Online tool to check server HTTP/2, ALPN, and Server-push support.');
  }

  protected function getForm() {
    $framework = $this->framework;
    $action = $framework->url('check');
    return <<<FORM
<form novalidate id="url-submit-form" class="control is-grouped" action="{$action}">
  <p class="control is-flex has-addons has-addons-centered is-expanded">
    <input required autocapitalize="off" spellcheck="false" class="input" type="url" name="url" placeholder="https://www.example.com">
    <input type="submit" class="button is-primary " value="Check" />
  </p>
</form>
FORM;
  }

  protected function initializeRender() {
    $this->render = new Html('page.html');
    $this->render->setVariable('is_front', FALSE);
    $this->render->setVariable('body_classes', 'is_page');
    $this->render->setVariable('header_content', $this->getForm());
  }

  abstract function getTitle();
  abstract function getBody();
}
