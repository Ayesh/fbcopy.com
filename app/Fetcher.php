<?php


namespace FBCopy;


use Curl\Curl;

class Fetcher {
  private $handler;
  private $video_id;
  private $access_token;
  private const API_VERSION = 'v2.9';

  public function __construct(int $video_id, $access_token) {
    $this->video_id = $video_id;

    $this->handler = new Curl();
    $this->handler
      ->setUserAgent('Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1')
      ->setReferer('https://www.facebook.com');

    $this->access_token = $access_token;
  }

  private function buildAPIUrl(): string {
    $query = [
      'access_token' => $this->access_token,
      'fields' => 'source,live_status,description,from,picture',
    ];
    $query = http_build_query($query);
    $url = 'https://graph.facebook.com/' .  static::API_VERSION . '/' . $this->video_id . '?' . $query;
    return $url;
  }

  public function getData(): array {
    $url = $this->buildAPIUrl();
    $this->handler->get($url);

    if ($this->handler->http_status_code === 200) {
      $data = json_decode($this->handler->response);
      return $this->buildFormatterData($data);
    }

    throw new \RuntimeException('Error fetching video data.');
  }

  private function buildFormatterData(\stdClass $data): array {
    $return = [];
    $return['id'] = $data->id ?? null;
    $return['source'] = $data->source ?? null;
    $return['description'] = $data->description ?? null;
    $return['is_live'] = (isset($data->live_status) && $data->live_status === 'LIVE');
    $return['author'] = $data->from->name ?? null;
    $return['thumbnail'] = $data->picture ?? null;
    return $return;
  }
}
