<?php

namespace Framework\Render;

class Json extends RenderInterface{
  protected $variables = [
    '_root' => NULL,
  ];

  public function render() {
    $this->emitResponseHeader();
    echo $this->getRender();
  }

  public function getRender() {
    $headers = $this->headers += $this->getDefaultHeaders();
    foreach ($headers as $header => $value) {
      header($header . ': ' . $value);
    }

    $variables = $this->variables += $this->getDefaultVars();
    if (isset($variables['_root'])) {
      return json_encode($variables['_root']);
    }
    return json_encode($variables);
  }

  protected function getDefaultVars() {
    return array(
      'compliments' => 'You are awesome!'
    );
  }

  protected function getDefaultHeaders() {
    return [
      'Content-type' => 'application/json; charset=utf-8'
    ];
  }

  public function setRootObject($value) {
    $this->setVariable('_root', $value);
  }
}
