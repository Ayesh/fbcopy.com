<?php


namespace FBCopy;

class Parser {
  private const STORY_PATH_PATH = '/story.php';
  private const STORY_PATH_FBID_KEY = 'story_fbid';
  private const VIDEO_PATH_PATH = '/videos/';

  private function __construct() {}

  /**
   * @param string $url
   *
   * @return int
   * @throws \InvalidArgumentException
   */
  public static function parseUrl(string $url): int {
    if (!filter_var($url, FILTER_VALIDATE_URL)) {
      throw new \InvalidArgumentException('Entered URL is invalid.');
    }

    $parts = parse_url($url);
    if (
      empty($parts['host'])
      || !in_array(strtolower($parts['host']), ['www.facebook.com', 'facebook.com', 'm.facebook.com'], true)
      || !in_array(strtolower($parts['scheme']), ['http', 'https'], true)) {
      throw new \InvalidArgumentException('Enter a facebook.com URL.');
    }

    if (empty($parts['path'])) {
      throw new \InvalidArgumentException('Entered URL is incorrect. Please copy and paste the video share URL.', 1);
    }

    if (strpos($parts['path'], static::VIDEO_PATH_PATH) !== false) {
      return static::validateVideoPath($parts);
    }
    if (!empty($parts['query']) && strpos($parts['path'], static::STORY_PATH_PATH) !== false) {
      return static::validateStoryPath($parts);
    }
    throw new \InvalidArgumentException('Entered URL is incorrect. Please copy and paste the video share URL.', 4);
  }

  /**
   * @param array $parts
   *
   * @return int
   * @throws \InvalidArgumentException
   */
  private static function validateStoryPath(array $parts): int {
    parse_str($parts['query'], $query);
    if (!empty($query[static::STORY_PATH_FBID_KEY]) && ctype_digit($query[static::STORY_PATH_FBID_KEY])) {
      return $query[static::STORY_PATH_FBID_KEY];
    }
    throw new \InvalidArgumentException('Entered URL is incorrect. Please copy and paste the video share URL.', 2);
  }

  /**
   * @param array $parts
   *
   * @return int
   * @throws \InvalidArgumentException
   */
  private static function validateVideoPath(array $parts): int {
    $path = $parts['path'];
    $path = explode('/', $path);
    foreach ($path as $index => $value) {
      $next = $index + 1;
      if ($value === 'videos' && isset($path[$next]) && ctype_digit($path[$next])) {
        return $path[$next];
      }
    }
    throw new \InvalidArgumentException('Entered URL is incorrect. Please copy and paste the video share URL.', 4);
  }

}
