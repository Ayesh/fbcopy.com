<?php


namespace Controllers;


use FBCopy\Parser;
use Framework\Render\Html;

class Front extends Base {

  protected function execute() {
    $this->render->setVariable('title', $this->framework->getSiteName());
    $url = $this->base->get('GET.url');
    if ($url) {
      try {
        $parser = Parser::parseUrl($url);
      }
      catch (\InvalidArgumentException $exception) {
        $url = '';
      }
    }

    $this->render->setVariable('title', $this->framework->getSetting('site_slogan'));
    $this->render->setVariable('form_default_url', $url);
    $this->render->setVariable('form_action_url', $this->framework->url('fetch'));
    $this->render->setVariable('form_csrf_value', $this->framework->csrfFields());
  }

  protected function initializeRender() {
    $this->render = new Html('page.html');
  }
}
