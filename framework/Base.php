<?php

namespace Framework;

use ArjanSchouten\HtmlMinifier\Minify;
use ArjanSchouten\HtmlMinifier\MinifyContext;
use ArjanSchouten\HtmlMinifier\PlaceholderContainer;

class Base {

  protected $configuration = [
    'debug' => 3,
    'ui' => '../templates/',
    'dist_key' => 0,
    'secret_key' => 'RHKG3Ayeshdsajkdq*(dsnRidmadjskq',
    'cdn' => FALSE,
  ];

  public $is_mock_run;
  public $base_url;

  public $base_path;
  public $doc_root;
  public $app_root;
  protected $base;

  protected $service_container;


  public function __construct(\Base $f3) {
    $this->initializeEnvironment();
    $this->base = $f3;
    $this->initializeVariables();
  }

  public function getSettings(): array {
    return $this->configuration;
  }

  public function getDistKey(): string {
    return $this->configuration['dist_key'];
  }

  public function getFromEmail(): string {
    return 'ayeshlakmal@gmail.com';
  }

  public function getSetting(string $setting_key, $default_value = null) {
    return $this->configuration[$setting_key] ?? $default_value;
  }

  public function getBaseUrl(): string {
    return $this->base_url;
  }

  public function getBasePath(): string {
    return $this->base_path;
  }

  public function getDocRoot(): string {
    return $this->doc_root;
  }

  public function getAppRoot(): string {
    return $this->app_root;
  }

  protected function initializeEnvironment(): void {
    $is_https = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) === 'on';
    $http_protocol = $is_https ? 'https' : 'http';
    $base_root = $http_protocol . '://' . $_SERVER['HTTP_HOST'];
    $base_url = $base_root;
    if ($dir = rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/')) {
      $base_path = $dir;
      $base_url .= $base_path;
      $base_path .= '/';
    }
    else {
      $base_path = '/';
    }

    $this->base_url = $base_url;
    $this->base_path = $base_path;
    $this->doc_root = defined('APP_ROOT') ? APP_ROOT : '';

    if (defined('APP_MAIN_ROOT')) {
      $this->app_root = APP_MAIN_ROOT;
    }
    else {
      $folder = explode(DIRECTORY_SEPARATOR, __DIR__);
      array_pop($folder); array_pop($folder);
      $this->app_root = implode(DIRECTORY_SEPARATOR, $folder);
    }

    $this->importSettings();
  }

  protected function importSettings() {
    /** @noinspection PhpIncludeInspection */
    $settings = include $this->doc_root . '/../config/settings.php';
    if ($settings) {
      $this->configuration = array_merge($this->configuration, $settings);

      if ($this->getEnvironment() === 'dev' && file_exists($this->doc_root . '/../config/dev.settings.php')) {
	      $settings = include $this->doc_root . '/../config/dev.settings.php';
	      if ($settings) {
		      $this->configuration = array_merge($this->configuration, $settings);
	      }
      }
    }
  }

  public function getEnvironment(): string {
  	if (getenv('PHP_DEVELOPMENT') === '1') {
  		return 'dev';
    }
    return 'prod';
  }

  public function isMockRun(): bool {
    return !empty($this->is_mock_run);
  }

  public function url(string $path = NULL, array $options = []): string {
    // Merge in defaults.
    $options += [
      'fragment' => '',
      'query' => [],
      'absolute' => FALSE,
      'external' => FALSE,
      'prefix' => ''
    ];

    if (isset($options['fragment']) && $options['fragment'] !== '') {
      $options['fragment'] = '#' . $options['fragment'];
    }

    if ($options['external']) {
      // Split off the fragment.
      if (strpos($path, '#') !== FALSE) {
        list($path, $old_fragment) = explode('#', $path, 2);
        // If $options contains no fragment, take it over from the path.
        if (isset($old_fragment) && !$options['fragment']) {
          $options['fragment'] = '#' . $old_fragment;
        }
      }
      // Append the query.
      if ($options['query']) {
        $path .= (strpos($path, '?') !== FALSE ? '&' : '?') . $this->http_build_query($options['query']);
      }
      // Reassemble.
      return $path . $options['fragment'];
    }

    $path = ltrim($path, '/');

    // The base_url might be rewritten from the language rewrite in domain mode.
    if (!isset($options['base_url'])) {
      $options['base_url'] = $this->getBaseUrl();
    }

    // The special path '<front>' links to the default front page.
    if ($path === '<front>') {
      $path = '';
    }

    $base = $options['absolute'] ? $options['base_url'] . '/' : $this->getBasePath();
    $prefix = empty($path) ? rtrim($options['prefix'], '/') : $options['prefix'];

    $path = str_replace('%2F', '/', rawurlencode($prefix . $path));
    if ($options['query']) {
      return $base . $path . '?' . $this->http_build_query($options['query']) . $options['fragment'];
    }
    return $base . $path . $options['fragment'];
  }

  private function http_build_query(array $query, $parent = '') {
    $params = [];

    foreach ($query as $key => $value) {
      $key = ($parent ? $parent . '[' . rawurlencode($key) . ']' : rawurlencode($key));

      // Recurse into children.
      if (is_array($value)) {
        $params[] = $this->http_build_query($value, $key);
      }
      // If a query parameter value is NULL, only append its key.
      elseif (!isset($value)) {
        $params[] = $key;
      }
      else {
        // For better readability of paths in query strings, we decode slashes.
        $params[] = $key . '=' . str_replace('%2F', '/', rawurlencode($value));
      }
    }

    return implode('&', $params);
  }

  public function getSiteName(): string {
    return $this->configuration['site_name'];
  }

  public static function checkPlain(string $text) {
    return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
  }

  public static function formatString(string $string, array $args = array()) {
    foreach ($args as $key => $value) {
      switch ($key[0]) {
        case '@':
          $args[$key] = static::checkPlain($value);
          break;

        case '%':
        default:
          $args[$key] = '<em class="placeholder">' . static::checkPlain($value) . '</em>';
          break;

        case '!':
          // Pass-through.
      }
    }
    return strtr($string, $args);
  }

  public static function hmacBase64($data, $key) {
    $hmac = base64_encode(hash_hmac('sha256', (string) $data, (string) $key, TRUE));
    // Modify the hmac so it's safe to use in URLs.
    return strtr($hmac, array('+' => '-', '/' => '_', '=' => ''));
  }

  public static function getPrivateKey(): string {
    return 'AyeshK';
  }

  protected function getDistPath(): string {
    return 'assets/dist/' . $this->getDistKey();
  }

  public function getAssetBase($cdn_url_only = FALSE): string {
    if (!empty($this->configuration['cdn'])) {
      if ($cdn_url_only) {
        return $this->configuration['cdn'];
      }
      $url_options = array(
        'base_url' => $this->configuration['cdn'],
        'absolute' => TRUE,
      );
    }
    else {
      if ($cdn_url_only) {
        return FALSE;
      }
      $url_options = array();
    }

    return $this->url($this->getDistPath() . '/', $url_options);
  }

  public function prefixDistPath(string $path): string {
    $base = $this->getAssetBase();
    return $base . $path;
  }

  public function preloadResource(string $path): void {
    $path = $this->prefixDistPath($path);
    header('Link: <' . $path .'>; rel=preload');
  }

  private function initializeVariables(): void {
    $f3 = $this->base;
    $f3->set('Framework', $this);

    $f3->set('UI', $this->configuration['ui']);
    $f3->set('DEBUG', $this->configuration['debug']);
    $f3->set('CORS', false);
    $f3->set('MinifyHtml', TRUE);
    /** @noinspection SpellCheckingInspection */
    $f3->set('ONERROR', '\Controllers\Errors\ErrorBase->handleError');
    $f3->set('TEMP', '../tmp/');

    $f3->set('NO_HEADERS', true);

    if ($this->configuration['debug'] < 1) {
      ini_set('display_errors', 0);
    }
  }

  public function csrfFields($given_key = FALSE) {
    $ip = $this->base->get('IP');
    $secret = $this->configuration['secret_key'];
    if ($given_key === FALSE) {
      $seed = base64_encode(random_bytes(8));
      return $seed . '/' . self::hmacBase64($seed . $ip, $secret);
    }

    $parts = explode('/', $given_key, 2);
    if (!isset($parts[1])) {
      return FALSE;
    }
    return hash_equals($parts[0] . '/' . self::hmacBase64($parts[0] .  $ip, $secret), $given_key);
  }

  /**
   * @param string $text
   *
   * @return string
   */
  public static function minifyHtml(string $text): string {
    $context = new MinifyContext(new PlaceholderContainer());
    $context->setContents($text);
    $minify = new Minify();
    $minify->run($context);
    $text = $context->getContents();
    return $text;
  }

  public function log($message): void {
    /** @noinspection ForgottenDebugOutputInspection */
    error_log($message);
  }
}
