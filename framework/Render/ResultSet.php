<?php

namespace Framework\Render;

use Framework\Base;
use Template;

class ResultSet extends RenderInterface{
  protected $render_type;
  protected $data = array();

  public function __construct($template) {
    parent::__construct($template);
    $this->template = 'result.html';
  }

  public function setRenderType($json = FALSE) {
    $this->render_type = $json;
  }

  public function setData(array $data) {
    $this->data = &$data;
  }

  protected function getDefaultVars() {
    return array();
  }

  public function render() {
    $this->getRender();
    if ($this->render_type) {
      $data = $this->renderJson();
      $data['result_rendered'] = $this->renderHtml();

      unset($data['report']);

      return $data;
    }
    else {
      return $this->renderHtml();
    }
  }

  public function getRender() {
    $vars = $this->data += $this->getDefaultVars();
    $vars += $this->variables;

    if (!empty($vars)) {
      $this->base->set('Report', $vars);
      $this->data['Meta'] = $meta = $this->getRenderMetaData();
      $this->base->mset($meta);
    }
  }

  protected function renderJson() {
    return $this->data;
  }

  protected function renderHtml() {
    if (empty($this->data)) {
      return $this->renderHtmlStub();
    }
    return $this->renderHtmlStub(Template::instance()->render($this->template));
  }

  protected function renderHtmlStub($content = '') {
    return <<<STUB
<div class="container" id="results">{$content}</div>
STUB;
  }

  protected function getRenderMetaData() {
    $framework = $this->framework;
    if (isset($this->data['report'])) {
      $report = $this->data['report'];
    }
    else {
      return [
        'History_Url' => $framework->url('check', array('absolute' => TRUE)),
        'meta_robots' => 'noindex, follow',
        'page_title' => $framework::formatString('@url - Error loading URL - HTTP2.Pro', array(
          '@url' => $this->data['url'],
        )),
        'favicon' => $this->framework->prefixDistPath('images/result-0.png'),
      ];
    }

    $return = [
      'History_Url' => $report['Meta']['Share']['Url'],
    ];

    $return['meta_keywords'] = $framework::formatString('HTTP/2 Status - @friendly_url, @friendly_url HTTP/2 support, @friendly_url HTTP2.pro report, HTTP/2 check report for @friendly_url.', array(
      '@friendly_url' => $report['Meta']['friendly_url'],
    ));

    $return['meta_robots'] = 'index, follow';

    if (empty($report['HTTP2']['status'])) {
      $return['favicon'] = $this->framework->prefixDistPath('images/result-0.png');

      $return['meta_description'] = $framework::formatString('@friendly_url does not HTTP/2 yet. How to add HTTP/2 support for @friendly_url.', array(
        '@friendly_url' => $report['Meta']['friendly_url'],
      ));

      $return['page_title'] = $framework::formatString('@friendly_url ✘ - HTTP/2 Not supported - HTTP2.Pro', array(
        '@friendly_url' => $report['Meta']['friendly_url'],
      ));
    }
    else {
      $return['favicon'] = $this->framework->prefixDistPath('images/result-1.png');

      $return['meta_description'] = $framework::formatString('@friendly_url supports HTTP/2. Full HTTP2.Pro report for @friendly_url.', array(
        '@friendly_url' => $report['Meta']['friendly_url'],
      ));

      $return['page_title'] = $framework::formatString('@friendly_url ✔ - HTTP/2 Supported - HTTP2.Pro', array(
        '@friendly_url' => $report['Meta']['friendly_url'],
      ));
    }

    return $return;
  }

  protected function getDefaultHeaders() {
    if ($this->render_type) {
      return [
        'Content-type' => 'application/json; charset=utf-8'
      ];
    }
    return [];
  }
}
