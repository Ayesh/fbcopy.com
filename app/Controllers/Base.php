<?php

namespace Controllers;

use Framework\Render\RenderInterface;

abstract class Base {
  /**
   * @var \Framework\Base
   */
  protected $framework;

  /**
   * @var RenderInterface
   */
  protected $render;

  /**
   * @var \Base
   */
  protected $base;

  abstract protected function execute();
  abstract protected function initializeRender();

  public function run(\Base $f3, $params = []) {
    $this->base = $f3;
    $start = microtime(TRUE);
    $this->framework = $f3->get('Framework');
    $this->initializeRender();
    $this->execute();
    $this->postExecute();
    $this->render();
    $end = microtime(TRUE);
    $this->render->setVariable('page_generate_time', $end - $start);
  }

  protected function render() {
    $this->render->render();
  }

  protected function postExecute() {

  }

}
