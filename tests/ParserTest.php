<?php

namespace FBCopy\Tests;

use FBCopy\Parser;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase {
	public function getNonUrlValues() {
		$values = [
			[''],
			[false],
			[[]],
			[new \stdClass()],
			['192.168.8.100'],
			['localhost'],
			['https://www.facebook.com:foo/videos/12343'],
		];

		return $values;
	}

	public function getNonFacebookUrls() {
		return [
			['https://ewe.facebook.com/dasjdk'],
			['https://ayesh.me'],
			['ftp://www.facebook.com'],
			['htxp://facebook.com'],
			['htxp://facebook.com'],
		];
	}

	public function getFacebookNonVideoUrls() {
		return [
			['https://www.facebook.com/ayeshkarunaratne'],
			['https://www.facebook.com/profile.php'],
			['https://www.facebook.com/profile.php?videos/foo/12343'],
			['https://www.facebook.com/profile.php#videos/foo/12343'],
			['https://www.facebook.com/#videos/foo/12343'],
			['https://www.facebook.com/#foo/videos/12343'],
			['https://www.facebook.com/?foo/videos/12343'],
			['https://www.facebook.com'],
      ['https://facebook.com/foo/videos/bar'],
      ['https://facebook.com/foo/videos/-7878554'],
      ['https://facebook.com/foo/videos/787.8554'],
      ['https://facebook.com/story.php'],
      ['https://facebook.com/profile.php'],
      ['https://facebook.com/story.php?story_id'],
      ['https://facebook.com/story.php?story_fbid'],
      ['https://facebook.com/story.php?story_fbid=false'],
      ['https://facebook.com/story.php?story_fbid=7854.54'],
      ['https://facebook.com/story.php?story_fbid=<em>xss</em>'],
      ['https://facebook.com/story.php?story_fbid=aa7854'],
		];
	}

	public function getValidUrls() {
	  return [
	    ['https://www.facebook.com/explosm/videos/10155258260425476/', 10155258260425476],
	    ['https://www.facebook.com/explosm/videos/10155258260425476', 10155258260425476],
      ['https://m.facebook.com/story.php?story_fbid=136630428436762672&id=1065654006827703', 136630428436762672],
    ];
  }

	public function testDirectConstructorCall() {
		$this->expectException(\Error::class);
		$this->expectExceptionMessage('Call to private FBCopy\Parser::__construct()');
		$class = Parser::class; // to bypass phpstorm error highlighting.
		$parser = new $class();
	}

	/**
	 * @dataProvider getNonUrlValues
	 *
	 * @param mixed $value
	 */
	public function testInvalidUrl($value) {
		if (is_string($value) || is_bool($value)) {
			$this->expectException(\InvalidArgumentException::class);
			$this->expectExceptionMessage('Entered URL is invalid.');
		}
		else {
			$this->expectException(\TypeError::class);
		}

		Parser::parseUrl($value);
	}

	/**
	 * @dataProvider getNonFacebookUrls
	 *
	 * @param mixed $value
	 */
	public function testNonFacebookUrls(string $value) {
		$this->expectException(\InvalidArgumentException::class);
		$this->expectExceptionMessage('Enter a facebook.com URL.');
		Parser::parseUrl($value);
	}

	/**
	 * @dataProvider getFacebookNonVideoUrls
	 *
	 * @param mixed $value
	 */
	public function testFacebookNonVideoUrls(string $value) {
		$this->expectException(\InvalidArgumentException::class);
		$this->expectExceptionMessage('Entered URL is incorrect. Please copy and paste the video share URL.');
		$video_id = Parser::parseUrl($value);
		$this->assertNull($video_id);
	}

	/**
	 * @dataProvider getFacebookNonVideoUrls
	 *
	 * @param mixed $value
	 */
	public function testFacebookNonVideoUrlsNoReturn(string $value) {
		try {
			$video_id = Parser::parseUrl($value);
		}
		catch (\InvalidArgumentException $exception) {
			$this->assertContains('Entered URL is incorrect. Please', $exception->getMessage());
			return;
		}
		$this->assertNull($video_id);
	}

  /**
   * @dataProvider getValidUrls
   *
   * @param string $url
   * @param int $fbid
   */
	public function testValidFacebookUrls(string $url, int $fbid) {
    $parsed_id = Parser::parseUrl($url);
    $this->assertSame($fbid, $parsed_id);
  }
}
