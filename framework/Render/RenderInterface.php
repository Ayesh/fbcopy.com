<?php

namespace Framework\Render;

use Framework\Base;

abstract class RenderInterface {
  /**
   * @var \Base
   */
  protected $base;

  /**
   * @var Base
   */
  protected $framework;

  /**
   * @var string
   */
  protected $template;

  protected $variables = [];
  protected $headers = [];

  /**
   * @var int
   */
  protected $response_code;

  public function __construct(string $template) {
    $this->base = \Base::instance();
    $this->template = $template;
    $this->framework = $this->base->get('Framework');
  }

  abstract protected function getDefaultVars();
  abstract protected function getDefaultHeaders();
  abstract public function render();


  public function setVariable($key, $value) {
    $this->variables[$key] = $value;
    return $this;
  }

  public function getVariable($key) {
    return $this->variables[$key] ?? null;
  }

  public function setReponseCode(int $status_code) {
    $this->response_code = $status_code;
  }

  public function setHeader($key, $value) {
    $this->headers[$key] = $value;
    return $this;
  }

  public function setVariableMultiple(array $variable_pairs) {
    $this->variables += $variable_pairs;
    return $this;
  }

  protected function baseVarMerge(array $vars) {
    foreach ($vars as $key => $var) {
      if (!$this->base->exists($key)) {
        $this->base->set($key, $var);
      }
    }
  }

  protected function preRender() {

  }

  final protected function emitResponseHeader() {
    $this->base->status($this->response_code);
  }
}
