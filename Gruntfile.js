module.exports = function(grunt) {
    require('time-grunt')(grunt);
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
            sass: {
                global: {
                  options: {
                    quiet: true,
                    style: 'expanded',
                    cacheLocation: '/tmp/.sass-cache',
                    sourcemap: 'none'
                  },
                  files: {                         // Dictionary of files
                    'web/assets/css/style.css': 'resources/sass/style.scss'
                  }
                }
            },
            uncss: {
                files: {
                    nonull: true,
                    src: [
                        'templates/_dummy.html',
                        'templates/error.html',
                        'templates/result.html',
                        'templates/page.html'
                    ],
                    dest: 'web/assets/css/style.css'
                },
                options: {
                    ignore: ['html', 'body', 'body.is_loading', '.button-primary.is-loading'],
                    stylesheets: ['../web/assets/css/style.css']
                }
            },
            cssmin: {
                options: {
                    report: 'gzip',
                    shorthandCompacting: false,
                    roundingPrecision: -1,
                    level: 0
                },
                global: {
                    files: [
                        {
                            expand: true,
                            cwd: 'web/assets/css',
                            src: ['*.css', '!*.min.css'],
                            dest: 'web/assets/css',
                            ext: '.min.css',
                            report: 'gzip'
                        }
                    ]
                }
            },
            uglify: {
                my_target: {
                    files: {
                        'web/assets/js/zepto.min.js': ['resources/js/zepto.min.js'],
                        'web/assets/js/script.min.js': ['resources/js/script.js', 'resources/js/analytics.js']
                    }
                }
            },
            copy: {
                main: {
                    expand: true,
                    cwd: 'resources/fonts',
                    src: '**',
                    dest: 'web/assets/fonts/'
                }
            }
    });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-uncss');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('default', ['sass', 'uncss', 'cssmin', 'uglify', 'copy']);
};
