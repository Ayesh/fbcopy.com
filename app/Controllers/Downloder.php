<?php


namespace Controllers;


use FBCopy\Fetcher;
use FBCopy\Parser;
use Framework\Render\Json;

class Downloder extends Base {

  private const DESCRIPTION_TRIM_LENGTH = 160;

  protected function execute(): void {
    $url = $this->base->get('REQUEST.url');
    $token = $this->base->get('REQUEST.token');

    /*if (!$this->validateToken($token)) {
      $this->render->setReponseCode(400);
      $this->framework->log('CSRF token mismatch');
      $this->render->setVariable('error', 'Please reload the page and try again.');
      return;
    }*/

    if (empty($url) || !is_string($url)) {
      $this->render->setReponseCode(400);
      $this->render->setVariable('error', 'Please enter the URL.');
      return;
    }

    $this->processFetch($url);
  }

  protected function initializeRender(): void {
    $this->render = new Json('result.html');
  }

  private function validateToken($token) {
    return $token && $this->framework->csrfFields($token);
  }

  private function processFetch(string $url) {
    try {
      $video_id = Parser::parseUrl($url);
    }
    catch (\InvalidArgumentException $exception) {
      $this->render->setReponseCode(400);
      $this->framework->log('Parser exception: '. $exception->getMessage());
      $this->render->setVariable('error', $exception->getMessage());
      return;
    }

    $fetcher = new Fetcher($video_id, $this->framework->getSetting('fb_secret'));

    try {
      $data = $fetcher->getData();
    }
    catch (\RuntimeException $exception) {
      $this->render->setReponseCode(400);
      $this->framework->log('Fetcher exception: '. $exception->getMessage());
      $this->render->setVariable('error', 'Could not fetch video information. Perhaps the video is private and not publicly accessible.');
      return;
    }

    $output = $this->getRenderedDownload($data);
    $this->render->setVariable('response_output', $output);
  }

  private function getRenderedDownload(array $data) {
    $this->preprocessData($data);
    foreach ($data as $key => $datum) {
      $this->base->set('result.' . $key, $datum);
    }
    return \Template::instance()->render('result.html');
  }

  protected function postExecute(): void {
    if ($this->render && $error = $this->render->getVariable('error')) {
      $this->render->setVariable('response_output', '<div class="response-error">' . $error . '</div>');
    }
  }

  private function preprocessData(array &$data) {
    if (isset($data['description']) && mb_strlen($data['description']) > static::DESCRIPTION_TRIM_LENGTH) {
      $data['description'] = mb_substr($data['description'], 0, static::DESCRIPTION_TRIM_LENGTH - 3) . '...';
    }
  }
}
