<?php

namespace FBCopy\Tests;

use FBCopy\Fetcher;
use FBCopy\Parser;
use PHPUnit\Framework\TestCase;

class FetcherTest extends TestCase {
	private $video_data = [];
	public function getVideoUrlsInvalid() {
		return $this->fetchVideoSet('invalid');
	}

	public function getVideoUrlsValid() {
		return $this->fetchVideoSet('valid');
	}

	private function fetchVideoSet($key) {
		require_once __DIR__ . '/data.php';
		$secret = facebook_api_key();
		$videos = $this->getVideoList();
    /** @noinspection ForeachSourceInspection */
    foreach ($videos[$key] as &$video) {
		  $video['key'] = $secret;
    }
		return $videos[$key];
	}

	private function getVideoList() {
		if (!$this->video_data) {
			require_once __DIR__ . '/data.php';
			$this->video_data = video_data();
		}

		return $this->video_data;
	}

	public function testFetcherInit() {
		$fetcher = new Fetcher(123, 'foobar');
		$this->assertInstanceOf(Fetcher::class, $fetcher);
	}

  /**
   * @dataProvider getVideoUrlsInvalid
   *
   * @param $url
   * @param $video_id
   * @param $secret
   */
	public function testInvalidVideoUrls($url, $video_id, $secret) {
		$parsed_id = Parser::parseUrl($url);
		$this->assertSame($video_id, $parsed_id);

		$fetcher = new Fetcher($video_id, $secret);
		$this->expectException(\RuntimeException::class);
		$this->expectExceptionMessage('Error fetching video data');
		$data = $fetcher->getData();
	}

  /**
   * @dataProvider getVideoUrlsValid
   *
   * @param $url
   * @param $video_id
   * @param $secret
   */
	public function testValidVideoUrls($url, $video_id, $secret) {
		$parsed_id = Parser::parseUrl($url);
		$this->assertSame($video_id, $parsed_id);

		$fetcher = new Fetcher($video_id, $secret);
		$data = $fetcher->getData();
		$this->assertEquals($data['id'], $video_id);
	}
}
