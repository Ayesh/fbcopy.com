<?php

require __DIR__ . '/../vendor/autoload.php';

$f3 = \Base::instance();
define('APP_ROOT', getcwd());
$config = new Framework\Base($f3);

require __DIR__ . '/../config/routes.php';

$f3->run();
