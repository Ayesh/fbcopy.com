<?php

namespace Controllers\Errors;

use Controllers\Base;
use Framework\Render\Html;
use Framework\Render\Json;

class ErrorBase extends Base {

  /**
   * @var \Base
   */
  protected $base;

  /**
   * @var Base
   */
  protected $framework;

  /**
   * @var array
   */
  protected $error;

  public function __construct() {
    $this->base = \Base::instance();
    $this->framework = $this->base->get('Framework');
  }

  public function handleError() {
    $f3 = $this->base;
    $framework = $this->framework;

    $this->error = &$f3->ref('ERROR');

    if ($this->error['code'] == 500 && $f3->get('DEBUG') < 1) {
      $error['text'] = $framework::formatString('An error occurred while processing your request. Sorry about that.');
    }
    $f3->status($this->error['code']);
    $this->run($f3);
  }


  protected function sendAjaxError(array &$error) {
    echo json_encode($error);
  }

  protected function sendHtmlError(array &$error) {
    $return = new Html('error.html');
  }

  protected function execute() {
    $this->render->setVariable('title', $this->error['status']);
    $this->render->setVariable('content_body', $this->error['text']);
    $this->render->setVariable('ErrorContents', $this->error);
    $this->render->setReponseCode($this->error['code']);
    if ($this->base->get('DEBUG') < 1) {
      $this->render->setVariable('ErrorContents.trace', '');
    }
  }

  protected function initializeRender() {
    if ($this->base->get('AJAX')) {
      $this->render = new Json('error.html');
    }
    else {
      $this->render = new Html('error.html');
    }
  }
}
