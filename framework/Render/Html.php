<?php

namespace Framework\Render;

use Framework\Base;
use Template;

class Html extends RenderInterface{

  /**
   * @var \Base
   * This is inherited from the parent, but used here to silent the lint.
   */
  protected $base;

  public function render() {
    $this->emitResponseHeader();
    echo $this->getRender();
  }

  public function getRender() {
    $vars = $this->variables += $this->getDefaultVars();
    $this->baseVarMerge($vars);
    return Template::instance()->render($this->template);
  }

  protected function getDefaultVars(): array {
    $framework = $this->framework;
    $vars = [
      'site_name' => $framework->getSiteName(),
      'site_slogan' => $framework->getSetting('site_slogan'),
      'base_path' => $framework->getBasePath(),
      'asset_path' => $framework->getAssetBase(),
      'favicon' => $framework->prefixDistPath($framework->getSetting('favicon')),
      'body_classes' => '',
      'content_body' => FALSE,
      'meta_keywords' => $framework->getSetting('site_keywords'),
      'meta_description' => $framework->getSetting('site_description'),
      'meta_robots' => NULL,
      'dns_preload' => $this->framework->getAssetBase(TRUE),
    ];

    if (empty($this->variables['is_front']) && isset($this->variables['title'])) {
      $vars['page_title'] = Base::formatString('@title - @sitename', array(
        '@title' => $this->variables['title'],
        '@sitename' => $vars['site_name'],
      ));
    }
    else {
      $vars['page_title'] = $vars['site_name'] . ' - ' . $vars['site_slogan'];
    }
    return $vars;
  }

  protected function getDefaultHeaders(): array {
    return [];
  }
}
