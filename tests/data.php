<?php

namespace FBCopy\Tests;

/**
 * @return array
 */
function video_data() {
	return [
		'valid' => [
		  [
        'url' => 'https://www.facebook.com/explosm/videos/10155258260425476/',
        'video_id' => 10155258260425476,
      ],
      [
        'url' => 'https://m.facebook.com/story.php?story_fbid=1366304286762672&id=1065654006827703',
        'video_id' => 1366304286762672,
      ],
		],
		'invalid' => [
		  [
        'url' => 'https://www.facebook.com/explosm/videos/101552582604254762',
        'video_id' => 101552582604254762,
      ],
      [
        'url' => 'https://m.facebook.com/story.php?story_fbid=136630428436762672&id=1065654006827703',
        'video_id' => 136630428436762672,
      ],
		]
	];
}

/**
 * @return string
 */
function facebook_api_key() {
	static $settings = null;
	if (!$settings) {
		$settings = require __DIR__ . '/../config/settings.php';
	}

	return $settings['fb_secret'];
}
